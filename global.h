/*-----------------------------------------------------------------------------
 **
 ** - slurp -
 **
 ** Pull RSS feeds into an SQlite DB
 **
 ** Copyright 2023-24 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/
#ifndef SLURP_GLOBAL_H
#define SLURP_GLOBAL_H

#include <string>

const std::string version{"v1.0.0"};

// program args are put into these
struct Configuration {
    std::string                 source{}; // URL to get the RSS / ATOM feed from
    std::string                 target{}; // path to sqlite3 DB
    bool                        verbose{false};
    bool                        version{false};
    uint16_t                    default_feed_key{0}; // feed key to be used if no specific is provided
};

#endif //SLURP_GLOBAL_H

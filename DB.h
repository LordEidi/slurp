/*-----------------------------------------------------------------------------
 **
 ** - slurp -
 **
 ** Pull RSS feeds into an SQlite DB
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/
#ifndef SLURP_DB_H
#define SLURP_DB_H

#include <string>
#include <vector>
#include <sqlite3.h>
#include <sstream>

#include "nlohmann/json.hpp"

struct Article {
    std::string pkey;
    uint16_t    feed_key;
    std::string legacy_id;
    std::string published_at;
    std::string updated_at;
    std::string source;
    std::string title;
    std::string lead;
    std::string text;
    std::string category;
    std::vector<std::string> categories;
    std::string crt;
    std::string upd;
};

struct Feed {
    std::string pkey;
    uint16_t    feed_key{0};
    std::string url;
    bool        is_active{true};
    uint16_t    cooldown_seconds{0};
    std::string user_agent;
    std::string poll_freq;
    uint8_t     fail_count{0};
    uint8_t     give_up_at{3};
    bool        truncate_parameters{false};
    std::string last_successful_poll_at;
    std::string last_error;
    std::string last_content;
    std::string crt;
    std::string upd;
};

enum class ARTICLE_FIELDS {
    PKEY = 0,
    FEED_KEY,
    LEGACY_ID,
    PUBLISHED_AT,
    UPDATED_AT,
    SOURCE,
    TITLE,
    LEAD,
    TEXT,
    CATEGORY,
    CRT,
    UPD
};

enum class FEED_FIELDS_SHORT {
    PKEY = 0,
    FEED_KEY,
    URL,
    USER_AGENT,
    POLL_FREQUENCY,
    FAIL_COUNT,
    GIVE_UP_AT,
    TRUNC_PARAMS
};

struct db_result {
    uint8_t     ret_code{SQLITE_OK};
    std::string message{""};
    std::string query{""};
};

class DB
{
public:
    DB(const std::string& db_path);

    db_result open_db();
    void close_db();

    db_result prepare_all_statements();
    void free_all_statements();

    db_result check_and_create_dbstructure();

    std::vector<Feed> getAllActiveFeeds();

    db_result insertArticle(const Article& a);
    db_result updateFeed(const Feed& f);

    db_result beginTransaction();
    db_result commitTransaction();
    db_result rollbackTransaction();

    static std::string getNowAsSQliteDate();

    sqlite3* getDB();

private:

    const std::string _db_path;
    sqlite3* _db;

    sqlite3_stmt* _ps_insert_article;
    sqlite3_stmt* _ps_load_active_feeds;
    sqlite3_stmt* _ps_update_feed;

    static Feed fillFeedFromRow(sqlite3_stmt* stmt);

    static void fillIfNotNullText(sqlite3_stmt* stmt, int column, std::string* field_to_fill);

    static std::string formatInsert(const std::string& field);
    static std::string formatInsert(const bool field);
};

#endif //SLURP_DB_H
// EOF

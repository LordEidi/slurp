/*-----------------------------------------------------------------------------
 **
 ** - slurp -
 **
 ** Pull RSS feeds into an SQlite DB
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/

#include "helper.h"

#include "log.h"
#include "uuid.h"

#include "date.h"
#include "fmt/chrono.h"

namespace helper::regex
{
    // 'Fri, 25 Aug 2023 06:49:00 -0500'
    RE2 date = RE2("(?P<wd>[[:alpha:]]{3}?), (?P<day>[0-9]{1,2}?) (?P<month>[[:alpha:]]{3}?) (?P<year>[0-9]{4}?) (?P<hour>[0-9]{2}?):(?P<min>[0-9]{2}?):(?P<sec>[0-9]{2}?) (?P<timezone>.*)");
    // 'Tue, 20 Feb 2024 18:08:46'
    RE2 date_short = RE2("(?P<wd>[[:alpha:]]{3}?), (?P<day>[0-9]{1,2}?) (?P<month>[[:alpha:]]{3}?) (?P<year>[0-9]{4}?) (?P<hour>[0-9]{2}?):(?P<min>[0-9]{2}?):(?P<sec>[0-9]{2}?)");
    // 'Fri, 25 Aug 2023 06:49 -0500'.
    RE2 date_nosecond = RE2("(?P<wd>[[:alpha:]]{3}?), (?P<day>[0-9]{1,2}?) (?P<month>[[:alpha:]]{3}?) (?P<year>[0-9]{4}?) (?P<hour>[0-9]{2}?):(?P<min>[0-9]{2}?) (?P<timezone>.*)");
    // 'Fri, 08/25/2023 - 02:15'
    RE2 date_strange_1 = RE2("(?P<wd>[[:alpha:]]{3}?), (?P<month>[0-9]{1,2}?)\\/(?P<day>[0-9]{1,2}?)\\/(?P<year>[0-9]{4}?) - (?P<hour>[0-9]{2}?):(?P<min>[0-9]{2}?)");

    // used to remove html tags, not perfect (misses recursions), but good enough for now
    RE2 htmltags = RE2("<(.|\n)*?>");

    // regex to change theregister URL
    RE2 url_rewrite_theregister = RE2(R"(go\.theregister\.com\/feed\/)");
}

// split convenience function
std::string split_last(const std::string &s, char delim)
{
    std::string ret{};

    auto pos = s.rfind(delim);
    if(pos > 0 && pos < s.size())
    {
        ret = s.substr(pos + 1);
    }

    return ret;
}

void remove_html(std::string* origin)
{
    RE2::GlobalReplace(origin, helper::regex::htmltags, "");
}

std::string calculate_uuid(const std::string& origin)
{
    uuids::uuid_name_generator gen(uuids::uuid::from_string(UUID_NAMESPACE).value());
    uuids::uuid const id = gen(origin);
    return to_string(id);
}

std::string format_date(const std::string& source)
{
    std::istringstream stream{source};
    std::chrono::time_point<std::chrono::system_clock> tp;
    stream >> date::parse("%a, %d %b %Y %H:%M:%S %z", tp);
    if (!stream.fail())
    {
        return fmt::format("{:%Y-%m-%d %H:%M:%S}", tp);
    }

    stream.clear();
    stream >> date::parse("%a, %d %b %Y %H:%M:%S %Z", tp);
    if (!stream.fail())
    {
        return fmt::format("{:%Y-%m-%d %H:%M:%S}", tp);
    }

    stream.clear();
    stream >> date::parse("%d %b %Y %H:%M:%S", tp);
    if (!stream.fail())
    {
        return fmt::format("{:%Y-%m-%d %H:%M:%S}", tp);
    }

    stream.clear();
    stream >> date::parse("%Y-%m-%dT%H:%M:%S", tp);
    if (!stream.fail())
    {
        return fmt::format("{:%Y-%m-%d %H:%M:%S}", tp);
    }

    std::string weekday;
    std::string day;
    std::string month;
    std::string year;
    std::string hour;
    std::string min;
    std::string sec;

    // 'Fri, 25 Aug 2023 06:49:00 -0500'.
    if (RE2::PartialMatch(source, helper::regex::date, &weekday, &day, &month, &year, &hour, &min, &sec))
    {
        std::istringstream stream2{fmt::format("{} {} {} {}:{}:{}", day, month, year, hour, min, sec)};
        stream2 >> date::parse("%d %b %Y %H:%M:%S", tp);
        if (!stream2.fail())
        {
            return fmt::format("{:%Y-%m-%d %H:%M:%S}", tp);
        }
    }

    // [*] 'Tue, 20 Feb 2024 18:08:46'
    if (RE2::PartialMatch(source, helper::regex::date_short, &weekday, &day, &month, &year, &hour, &min, &sec))
    {
        std::istringstream stream2{fmt::format("{} {} {} {}:{}:{}", day, month, year, hour, min, sec)};
        stream2 >> date::parse("%d %b %Y %H:%M:%S", tp);
        if (!stream2.fail())
        {
            return fmt::format("{:%Y-%m-%d %H:%M:%S}", tp);
        }
    }

    // 'Fri, 25 Aug 2023 06:49 -0500'.
    if (RE2::PartialMatch(source, helper::regex::date_nosecond, &weekday, &day, &month, &year, &hour, &min))
    {
        std::istringstream stream2{fmt::format("{} {} {} {}:{}:{}", day, month, year, hour, min, "00")};
        stream2 >> date::parse("%d %b %Y %H:%M:%S", tp);
        if (!stream2.fail())
        {
            return fmt::format("{:%Y-%m-%d %H:%M:%S}", tp);
        }
    }

    // [*] 'Fri, 08/25/2023 - 02:15'.
    if (RE2::PartialMatch(source, helper::regex::date_strange_1, &weekday, &month, &day, &year, &hour, &min))
    {
        std::istringstream stream2{fmt::format("{} {} {} {}:{}:{}", day, month, year, hour, min, "00")};
        stream2 >> date::parse("%d %m %Y %H:%M:%S", tp);
        if (!stream2.fail())
        {
            return fmt::format("{:%Y-%m-%d %H:%M:%S}", tp);
        }
    }

    warning("Failed to parse date. Giving up. Source '{}'.", source);

    // TODO fix me sometimes
    return "1999-01-01 00:00:00"; // yeah, not cool, but ok for now
}

std::string get_url(mrss_tag_t* tag)
{
    while (tag)
    {
        if(strcmp(tag->name, "url") == 0)
        {
            std::string url{tag->value};
            return url;
        }

        tag = tag->next;
    }

    return "";
}

static void print_tags (mrss_tag_t* tag, int index)
{
    mrss_attribute_t *attribute;
    int i;

    for (i = 0; i < index; i++)
        fprintf (stdout, "\t");

    fprintf (stdout, "Other Tags:\n");
    while (tag)
    {
        for (i = 0; i < index; i++)
            fprintf (stdout, "\t");

        fprintf (stdout, "\ttag name: %s\n", tag->name);

        for (i = 0; i < index; i++)
            fprintf (stdout, "\t");

        fprintf (stdout, "\ttag value: %s\n", tag->value);

        for (i = 0; i < index; i++)
            fprintf (stdout, "\t");

        fprintf (stdout, "\ttag ns: %s\n", tag->ns);

        if (tag->children)
            print_tags (tag->children, index + 1);

        for (attribute = tag->attributes; attribute;
             attribute = attribute->next)
        {
            for (i = 0; i < index; i++)
                fprintf (stdout, "\t");

            fprintf (stdout, "\tattribute name: %s\n", attribute->name);

            for (i = 0; i < index; i++)
                fprintf (stdout, "\t");

            fprintf (stdout, "\tattribute value: %s\n", attribute->value);

            for (i = 0; i < index; i++)
                fprintf (stdout, "\t");

            fprintf (stdout, "\tattribute ns: %s\n", attribute->ns);
        }

        tag = tag->next;
    }
}

bool rewrite_url(std::string* url)
{
    bool changed{false};

    // theregister uses: https://go.theregister.com/feed/www.theregister.com/year/month/day/title/ -> https://www.theregister.com/year/month/day/title/
    changed = RE2::GlobalReplace(url, helper::regex::url_rewrite_theregister, "");

    return changed;
}

// EOF
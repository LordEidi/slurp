cmake_minimum_required(VERSION 3.25)

project(slurp)

# libcpr wants to build itself dynamically, me want static
# change this if you prefer a dynamically linked libcpr
set(BUILD_SHARED_LIBS OFF)

set(CMAKE_CXX_STANDARD 20)

set(CPM_USE_LOCAL_PACKAGES true)

set(CPM_DOWNLOAD_VERSION 0.40.5)
set(CPM_DOWNLOAD_LOCATION "${CMAKE_BINARY_DIR}/cmake/CPM_${CPM_DOWNLOAD_VERSION}.cmake")

if(NOT (EXISTS ${CPM_DOWNLOAD_LOCATION}))
    message(STATUS "Downloading CPM.cmake")
    file(DOWNLOAD https://github.com/TheLartians/CPM.cmake/releases/download/v${CPM_DOWNLOAD_VERSION}/CPM.cmake ${CPM_DOWNLOAD_LOCATION})
endif()

include(FindPkgConfig)

find_package(fmt REQUIRED)

#pkg_check_modules(LIBMRSS REQUIRED mrss)
pkg_check_modules(SQLITE3 REQUIRED sqlite3)
pkg_check_modules(RE2 REQUIRED re2)

add_executable(${PROJECT_NAME} CLI11.hpp uuid.h date.h global.h DB.h DB.cpp helper.cpp helper.h log.cpp log.h main.cpp)

# add dependencies
include(${CPM_DOWNLOAD_LOCATION})

CPMAddPackage("gh:bakulf/libmrss#0.19.4")

#CPMAddPackage("gh:libcpr/cpr#1.11.1")
CPMAddPackage(
        NAME cpr
        GIT_TAG 1.11.1
        GITHUB_REPOSITORY libcpr/cpr
        OPTIONS
        "CURL_USE_LIBPSL OFF"
)

target_link_libraries(${PROJECT_NAME} PRIVATE cpr)
target_link_libraries(${PROJECT_NAME} PRIVATE mrss)

target_link_libraries(${PROJECT_NAME} PRIVATE ${RE2_LIBRARIES})
#target_link_libraries(${PROJECT_NAME} PRIVATE ${LIBMRSS_LIBRARIES})
target_link_libraries(${PROJECT_NAME} PRIVATE ${SQLITE3_LIBRARIES})
target_link_libraries(${PROJECT_NAME} PRIVATE fmt::fmt)

# slurp

[![status-badge](https://ci.codeberg.org/api/badges/12446/status.svg)](https://ci.codeberg.org/12446)

## What?

Ingest RSS feeds into a local SQLite3 database with a flat structure. 

Primary keys are UUIDv5 formatted either from the GUID of the processed feed or from the URL if not exists. Syncing two or more *slurp* databases should be a breeze.

## Why?

Why not? We needed some way to quickly ingest RSS and ATOM feeds and store them in a simple format to be further processed.

## How?

### Install and use

To run *slurp*, you will have to install these fine libraries:

````
sudo apt install libfmt libmrss libsqlite3 libfmt7 libre2-9
````

Then head to our download page, get the binary of *slurp* and place it somewhere on your system.

To run *slurp*, use this example:

````
./slurp --source <your RSS feed> --target ./your.sqlitedb
````

The database file will be created if it does not exist.

Run this command within your sqlite3 client with the newly created database for details:

````
.schema
````

For details of how to use *slurp*, see:

````
./slurp --help
````


### Compile for yourself

````
sudo apt install libfmt-dev libmrss0-dev libsqlite3-dev libre2-dev

cmake -DCMAKE_BUILD_TYPE=Release -S . -B build
cmake --build build --target slurp j 6
````


## Shoulders of Giants

*slurp* makes use of the following software by different authors and with their respective (c):

- SQLite3
- CLI11
- libmrss
- fmt
- RE2
- date.h
- stduuid

## Licence
Copyright (C) 2023 SwordLord - the coding crew. Please see enclosed LICENCE file
for details.

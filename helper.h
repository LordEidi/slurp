/*-----------------------------------------------------------------------------
 **
 ** - slurp -
 **
 ** Pull RSS feeds into an SQlite DB
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/

#ifndef SLURP_HELPER_H
#define SLURP_HELPER_H

#include <string>
#include <sqlite3.h>
#include "mrss.h"
#include "re2/re2.h"

#define UUID_NAMESPACE "bada55e5-f33d-c475-d095-7a911a7e11e5"

std::string get_url (mrss_tag_t* tag);
static void print_tags (mrss_tag_t* tag, int index);

void remove_html(std::string* origin);

std::string split_last(const std::string& s, char delim);
std::string calculate_uuid(const std::string& origin);

std::string format_date(const std::string& source);

bool rewrite_url(std::string* url);

static inline void replace_all(std::string& target, const std::string& search, const std::string& replace)
{
    size_t start_pos = 0;
    while((start_pos = target.find(search, start_pos)) != std::string::npos)
    {
        target.replace(start_pos, search.length(), replace);
        start_pos += replace.length(); // Handles case where 'replace' is a substring of 'search'
    }
}

#endif //SLURP_HELPER_H

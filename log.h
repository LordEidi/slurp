/*-----------------------------------------------------------------------------
 **
 ** - slurp -
 **
 ** Pull RSS feeds into an SQlite DB
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/

#ifndef SLURP_LOG_H
#define SLURP_LOG_H

#include "fmt/core.h"
#include "fmt/color.h"
#include "fmt/printf.h"

#include "global.h"

extern Configuration conf;

template<typename S, typename... Args>
inline void error(const S s, Args&&... args)
{
    std::string message{s};
    fmt::print(stderr, fg(fmt::color::indian_red), "[!] " + message + "\n", args...);
}

template<typename S, typename... Args>
inline void warning(const S s, Args&&... args)
{
    std::string message{s};
    fmt::print(stderr, fg(fmt::color::orange), "[*] " + message + "\n", args...);
}

inline void warning(const std::string s)
{
    warning(s, nullptr);
}

template<typename S, typename... Args>
inline void info(const S s, Args&&... args)
{
    if(conf.verbose)
    {
        std::string message{s};
        fmt::print(stderr, fg(fmt::color::alice_blue), "[-] " + message + "\n", args...);
    }
}

inline void info(const std::string s)
{
    info(s, nullptr);
}

template<typename S, typename... Args>
inline void die(const S s, Args&&... args)
{
    std::string message{s};
    fmt::print(stderr, fg(fmt::color::indian_red), "[!] " + message + "\n", args...);
}

inline void die(const std::string s)
{
    die(s, nullptr);
}

#endif //SLURP_LOG_H

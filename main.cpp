/*-----------------------------------------------------------------------------
 **
 ** - slurp -
 **
 ** Pull RSS feeds into an SQlite DB
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/
#include <unistd.h>
#include "mrss.h"

#include <sqlite3.h>
#include <cpr/cpr.h>

#include "CLI11.hpp"
#include "global.h"
#include "helper.h"
#include "DB.h"

#include "log.h"

Configuration conf{};

// example of source URL: -s https://mastodon.social/tags/cyber.rss

int main(int argc, char **argv)
{
    CLI::App app("slurp");

    app.add_option("-s,--source", conf.source, "URL for the feed to be ingested.");
    app.add_option("-t,--target", conf.target, "Path to the sqlite DB to write the data into.");
    app.add_option("-f,--feed-key", conf.default_feed_key, "Feed-Key to store articles with.");

    app.add_flag("-v, --verbose", conf.verbose, "Reports lots of extra debugging information about what is going on.");
    app.add_flag("-V, --version", conf.version, "Display the version info and quit.");

    app.get_config_formatter_base()->quoteCharacter('"', '"');

    CLI11_PARSE(app, argc, argv)

    // show version info and quit
    if(conf.version)
    {
        // make sure to see something
        conf.verbose = true;

        info(R"(░▄▀▀░█▒░░█▒█▒█▀▄▒█▀▄)");
        info(R"(▒▄██▒█▄▄░▀▄█░█▀▄░█▀▒)");

        info("slurp {}", version);
        info("(c) 2023-24 by SwordLord - the coding crew");
        return 0;
    }

    if(conf.source.empty() || conf.target.empty())
    {
        die("--source and --target may not be empty.");
        return 2;
    }

    // make sure to have absolute paths from user input
    std::filesystem::path _pTarget = std::filesystem::path(conf.target);
    try
    {
        if(std::filesystem::exists(_pTarget))
        {
            // if it is relative, make sure that there is no bloat in the path
            if(_pTarget.is_relative())
            {
                conf.target = std::filesystem::canonical(_pTarget);
            }
        }
        else
        {
            std::filesystem::path p = std::filesystem::weakly_canonical(_pTarget);

            // make sure directories exist if file does not exist
            std::filesystem::create_directories(p.parent_path());

            conf.target = p;

            // TODO should we create the file if not existing? Or is the implicit creation of libsqlite good enough for us?
        }
    }
    catch(const std::exception& ex)
    {
        die("validating --target for {} threw an exception: {}.", conf.target, ex.what());
        return 1;
    }

    // initialise database
    DB db{conf.target};

    db_result ret = db.open_db();
    if(ret.ret_code != SQLITE_OK)
    {
        die("Error opening database: {} - {}", ret.ret_code, ret.message);
        return 1;
    }

    // sanity checks and structure creation, if needed
    ret = db.check_and_create_dbstructure();
    if(ret.ret_code != SQLITE_OK)
    {
        die("Error creating tables: {} - {}", ret.ret_code, ret.message);
        db.close_db();
        return 1;
    }

    // make sure to have all prepared statements ready
    ret = db.prepare_all_statements();
    if(ret.ret_code != SQLITE_OK)
    {
        die("Error preparing statements: {} - {}", ret.ret_code, ret.message);
        db.close_db();
        return 1;
    }

    /*
    opts = mrss_options_new(5,
                            nullptr,
                            nullptr,
                            nullptr,
                            nullptr,
                            nullptr,
                            1,
                            nullptr,
                            user_agent.data()); // .data() returns char* while .c_str() returns const char*... C++17
    */

    std::vector<Feed> feeds;

    if(conf.source == "db")
    {
        feeds = db.getAllActiveFeeds();
    }
    else
    {
        Feed f;
        f.url = conf.source;
        f.feed_key = conf.default_feed_key;
        feeds.push_back(f);
    }

    for (auto f : feeds)
    {
        mrss_t* data;
        mrss_category_t* cat;
        mrss_item_t* item;

        //
        //
        //mrss_options_t* opts;

        if (f.url.starts_with("http://") || f.url.starts_with("https://"))
        {
            // some servers are picky re user agent...
            std::string user_agent{"slurp/" + version};

            // user agent can be overridden per feed
            if(!f.user_agent.empty())
            {
                user_agent = f.user_agent;
            }

            // Test to rewrite URLs
            // auto c = curl_url();
            // auto rc = curl_url_set(c, CURLUPART_URL, "https://example.com:449/foo/bar?name=moo", 0);

            cpr::Response r = cpr::Get(cpr::Url{f.url}, cpr::Header{{"User-Agent", user_agent}});
            if (r.status_code == 200)
            {
                mrss_error_t err = mrss_parse_buffer(const_cast<char *>(r.text.c_str()), r.text.length(), &data);
                if (err)
                {
                    std::string s_err = mrss_strerror(err);
                    error("MRSS return error: {}", s_err);

                    // TODO have some logic which deactivates the feed if the fail_count is bigger or equal to give_up_at
                    f.fail_count += 1;
                    f.last_error = s_err;
                    f.last_content = r.text;
                    db.updateFeed(f);
                    // TODO tell user if that fails as well
                    continue;
                }
                else
                {
                    // if http status == 200 AND no error during parsing
                    // BUT there was an error before, reset fail count
                    if(f.fail_count > 0)
                    {
                        info("resetting error count for feed: {}", f.pkey);
                        // the question is, if we should reset last_error and last_content as well.
                        // if we do so, we lose any intel on what once happened with a feed
                        // if we don't, we have potentially irrelevant intel in the record
                        // currently tending to delete
                        f.fail_count = 0;
                        f.last_error = "";
                        f.last_content = "reset the fail_count at " + DB::getNowAsSQliteDate();
                        db.updateFeed(f);
                    }
                }
            }
            else
            {
                error("CPR returned an error: {} {}", r.status_code, r.error.message);
                f.fail_count += 1;
                f.last_error = r.error.message;
                f.last_content = r.text;
                // TODO tell user if that fails as well
                db.updateFeed(f);
                continue;
            }
        }
        else
        {
            mrss_error_t err = mrss_parse_file(const_cast<char *>(f.url.c_str()), &data);
            if (err)
            {
                std::string s_err = mrss_strerror (err);
                error("MRSS return error while parsing local file: {}", s_err);

                f.fail_count += 1;
                f.last_error = s_err;
                f.last_content = "";
                // TODO tell user if that fails as well
                db.updateFeed(f);
                continue;
            }
        }

        // downloading and parsing succeeded, lets process the records.
        // we fill a vector with articles first, to not block the db for too long
        std::vector<Article> articles;
        std::string s_legacy_id{0};

        item = data->item;
        while (item)
        {
            std::string category;
            cat = item->category;
            while (cat)
            {
                if(!category.empty())
                {
                    category += ",";
                }
                category += cat->category;
                cat = cat->next;
            }

            std::string title;
            if(item->title != nullptr)
            {
                title += item->title;
            }

            // libmrss has problems with HTML formatted description fields
            std::string lead;
            std::string body;
            if(item->description != nullptr)
            {
                body = item->description;

                remove_html(&body);

                if(body.length() > 1500)
                {
                    // find the last space to not risk to cut through a character (ending in a non UTF8 character)
                    auto last = body.find_last_of(' ', 1495);
                    body = body.substr(0, last) + "[...]";
                }
            }

            if(title.empty() && body.length() > 200)
            {
                // find the last space to not risk to cut through a character (ending in a non UTF8 character)
                auto last = body.find_last_of(' ', 200);
                title = body.substr(0, last) + "[...]";
            }

            std::string pointing_to; // the string to be used as url/source
            if(item->link != nullptr)
            {
                pointing_to = item->link;
            }
            else
            {
                std::string url = get_url(item->other_tags);
                if (!url.empty())
                {
                    pointing_to = url;
                }
                else
                {
                    warning("Error finding an URL as source of article. Giving up, not processing item {}", title);
                    continue;
                }
            }

            // rewrite url if need be
            rewrite_url(&pointing_to);

            std::string identifier; // the value to be used as pkey
            if(item->guid != nullptr)
            {
                identifier = item->guid;
            }
            else
            {
                identifier = pointing_to;
            }

            std::string pkey = calculate_uuid(identifier);

            // remove everything after the ? mark (inclusive),
            // only do so on feeds using URLs not using a param to identify the target
            if(f.truncate_parameters)
            {
                auto loc_of_qmark = pointing_to.find('?', 0);
                if(loc_of_qmark != std::string::npos)
                {
                    pointing_to = pointing_to.substr(0, loc_of_qmark);
                }
            }

            // Although there are standards and definitions, many sources
            // just don't care. Deliver nothing or something.
            // And we need %Y-%m-%d %H:%M:%S
            // So let's do some magic...
            std::string published;
            if(item->pubDate != nullptr)
            {
                published = format_date(item->pubDate);
            }
            else
            {
                published = DB::getNowAsSQliteDate();
            }

            Article a;
            a.pkey = pkey;
            a.feed_key = f.feed_key;
            a.legacy_id = s_legacy_id;
            a.published_at = published;
            a.updated_at = published;
            a.source = pointing_to;
            a.title = title;
            a.lead = lead;
            a.text = body;
            a.category = category;

            articles.push_back(a);

            item = item->next;
        }

        if(!articles.empty())
        {
            ret = db.beginTransaction();
            if (ret.ret_code != SQLITE_OK)
            {
                error("SQL Error, begin transaction: {} - {}", ret.ret_code, ret.message);
            }

            for(const auto& a: articles)
            {
                db_result res = db.insertArticle(a);
                if(res.ret_code == SQLITE_DONE)
                {
                    info("Record touched: {} - {}", a.pkey, a.source);
                }
                else
                {
                    error("SQL Error, insert article: {} - {}", res.ret_code, res.message);
                }
            }

            ret = db.commitTransaction();
            if (ret.ret_code != SQLITE_OK)
            {
                error("SQL Error, commit transaction: {} - {}", ret.ret_code, ret.message);
            }
        }

        mrss_free(data);

        bool was_verbose = conf.verbose;
        conf.verbose = true;
        info("Processed {} records on '{}'.", articles.size(), f.url);
        conf.verbose = was_verbose;

        // sleep n seconds if cooldown requested.
        // Usually needed on domains which don't like if you quickly get multiple feeds from the same domain (although hitting different feeds / URLs)
        if(f.cooldown_seconds > 0)
        {
            sleep(f.cooldown_seconds);
        }
    }

    db.free_all_statements();
    db.close_db();

    return 0;
}
// EOF
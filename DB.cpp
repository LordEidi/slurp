/*-----------------------------------------------------------------------------
 **
 ** - slurp -
 **
 ** Pull RSS feeds into an SQlite DB
 **
 ** Copyright 2023 by SwordLord - the coding crew - http://www.swordlord.com
 ** and contributing authors
 **
 ** This program is free software; you can redistribute it and/or modify it
 ** under the terms of the GNU Affero General Public License as published by the
 ** Free Software Foundation, either version 3 of the License, or (at your option)
 ** any later version.
 **
 ** This program is distributed in the hope that it will be useful, but WITHOUT
 ** ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 ** FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 ** for more details.
 **
 ** You should have received a copy of the GNU Affero General Public License
 ** along with this program. If not, see <http://www.gnu.org/licenses/>.
 **
 **-----------------------------------------------------------------------------
 **
 ** Original Authors:
 ** LordEidi@swordlord.com
 **
-----------------------------------------------------------------------------*/
#include <chrono>
#include "fmt/chrono.h"
#include "DB.h"
#include "log.h"
#include "global.h"

DB::DB(const std::string& db_path) : _db_path(db_path)
{
}

db_result DB::open_db()
{
    db_result ret;

    ret.ret_code = sqlite3_open_v2(_db_path.c_str(),
                                   &_db,
                                   SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_WAL,
                                   nullptr);
    if (ret.ret_code != SQLITE_OK)
    {
        ret.message = sqlite3_errmsg(_db);
        return ret;
    }

    sqlite3_busy_timeout(_db, 2 * 1000);

    // make sure db is in WAL journal mode

    sqlite3_stmt* ps_pragma;
    std::string sql = "pragma journal_mode;";
    ret.ret_code = sqlite3_prepare_v3(_db, sql.c_str(), sql.length(), 0, &ps_pragma, nullptr);
    if (ret.ret_code != SQLITE_OK)
    {
        ret.message = sqlite3_errmsg(_db);
        ret.query = sql;
        return ret;
    }

    ret.ret_code = sqlite3_step(ps_pragma);
    if(ret.ret_code != SQLITE_ROW)
    {
        ret.message = sqlite3_errmsg(_db);
        ret.query = sqlite3_sql(ps_pragma);
        return ret;
    }

    bool isWAL{false};
    const char* val = reinterpret_cast<const char*>(sqlite3_column_text(ps_pragma, 0));
    if(val != nullptr)
    {
        isWAL = strcmp("wal",val) == 0;
    }
    sqlite3_finalize(ps_pragma);

    if(!isWAL)
    {
        std::string s_wal_mode = "pragma journal_mode = WAL;";
        char* messageError;

        ret.ret_code = sqlite3_exec(_db, s_wal_mode.c_str(), nullptr, nullptr, &messageError);
        if (ret.ret_code != SQLITE_OK)
        {
            ret.message = messageError;
            ret.query = s_wal_mode;
            sqlite3_free(messageError);
            return ret;
        }
    }

    ret.ret_code = SQLITE_OK;
    return ret;
}

void DB::close_db()
{
    // make sure to regularly write WAL into db
    int pnLog;  // Size of WAL log in frames
    int pnCkpt; // Total number of frames checkpointed
    sqlite3_wal_checkpoint_v2(_db, nullptr, SQLITE_CHECKPOINT_PASSIVE, &pnLog, &pnCkpt);
    info("DB Checkpoint. Processed {} / {}", pnCkpt, pnLog);
    sqlite3_close(_db);
}

sqlite3* DB::getDB()
{
    return _db;
}

db_result DB::prepare_all_statements()
{
    db_result ret;

    std::string sql{"SELECT pkey, feed_key, url, user_agent, poll_freq, fail_count, give_up_at, trunc_params FROM feeds WHERE is_active != 0 AND fail_count < give_up_at ORDER BY feed_key ASC;"};
    ret.ret_code = sqlite3_prepare_v3(_db, sql.c_str(), sql.length(), SQLITE_PREPARE_PERSISTENT, &_ps_load_active_feeds, nullptr);
    if (ret.ret_code != SQLITE_OK)
    {
        ret.message = sqlite3_errmsg(_db);
        ret.query = sql;
        return ret;
    }

    sql = "UPDATE feeds SET fail_count = $fail_count, last_error = $last_error, last_content = $last_content, upd = CURRENT_TIMESTAMP WHERE pkey = $pkey;";
    ret.ret_code = sqlite3_prepare_v3(_db, sql.c_str(), sql.length(), SQLITE_PREPARE_PERSISTENT, &_ps_update_feed, nullptr);
    if (ret.ret_code != SQLITE_OK)
    {
        ret.message = sqlite3_errmsg(_db);
        ret.query = sql;
        return ret;
    }

    std::stringstream sts;
    sts << "INSERT INTO articles VALUES(";
    sts << "$pkey,";
    sts << "$feed_key,";
    sts << "$legacy_id,";
    sts << "$published_at,";
    sts << "$updated_at,";
    sts << "$source,";
    sts << "$title,";
    sts << "$lead,";
    sts << "$text,";
    sts << "$category,";
    sts << "CURRENT_TIMESTAMP,";
    sts << "CURRENT_TIMESTAMP";
    sts << ") ";

    sts << "ON CONFLICT(pkey) DO UPDATE SET ";

    sts << "updated_at=$published,";
    sts << "title=$title,";
    sts << "text=$text,";
    sts << "category=$category,";
    sts << "upd=CURRENT_TIMESTAMP";

    sts << ";";

    sql = sts.str();
    ret.ret_code = sqlite3_prepare_v3(_db, sql.c_str(), sql.length(), SQLITE_PREPARE_PERSISTENT,  &_ps_insert_article, nullptr);
    if (ret.ret_code != SQLITE_OK)
    {
        ret.message = sqlite3_errmsg(_db);
        ret.query = sql;
        return ret;
    }

    return ret;
}

// the calling procedure is responsible to free data from prepare statements
void DB::free_all_statements()
{
    sqlite3_finalize(_ps_load_active_feeds);
    sqlite3_finalize(_ps_insert_article);
    sqlite3_finalize(_ps_update_feed);
}

db_result DB::check_and_create_dbstructure()
{
    db_result ret;
    char* messageError;

    std::string s_ct_articles = "CREATE TABLE IF NOT EXISTS `articles` ("
                                "`pkey` TEXT UNIQUE PRIMARY KEY, "
                                "`feed_key` INTEGER DEFAULT 0, "
                                "`legacy_id` TEXT, "
                                "`published_at` TEXT NOT NULL, "
                                "`updated_at` TEXT, "
                                "`source` TEXT, "
                                "`title` TEXT, "
                                "`lead` TEXT, "
                                "`text` TEXT, "
                                "`category` TEXT, "
                                "`crt` TEXT DEFAULT CURRENT_TIMESTAMP, "
                                "`upd` TEXT DEFAULT CURRENT_TIMESTAMP);";

    ret.ret_code = sqlite3_exec(_db, s_ct_articles.c_str(), nullptr, nullptr, &messageError);
    if (ret.ret_code != SQLITE_OK)
    {
        ret.message = messageError;
        ret.query = s_ct_articles;
        sqlite3_free(messageError);
        return ret;
    }

    std::string s_ct_feeds = "CREATE TABLE IF NOT EXISTS `feeds` ("
                             "    `pkey` TEXT UNIQUE PRIMARY KEY, "
                             "    `feed_key` INTEGER DEFAULT 0, "
                             "    `url` TEXT UNIQUE, "
                             "    `is_active` INTEGER DEFAULT 1, "
                             "    `user_agent` TEXT DEFAULT '', "
                             "    `poll_freq` TEXT DEFAULT '', "
                             "    `fail_count` INTEGER DEFAULT 0, "
                             "    `give_up_at` INTEGER DEFAULT 3, "
                             "    `trunc_params` INTEGER DEFAULT 0, "
                             "    `last_successful_poll_at` TEXT, "
                             "    `last_error` TEXT DEFAULT '', "
                             "    `last_content` TEXT DEFAULT '', "
                             "    `crt` TEXT DEFAULT CURRENT_TIMESTAMP, "
                             "    `upd` TEXT DEFAULT CURRENT_TIMESTAMP);";

    ret.ret_code = sqlite3_exec(_db, s_ct_feeds.c_str(), nullptr, nullptr, &messageError);
    if (ret.ret_code != SQLITE_OK)
    {
        ret.message = messageError;
        ret.query = s_ct_feeds;
        sqlite3_free(messageError);
        return ret;
    }

    return ret;
}

std::vector<Feed> DB::getAllActiveFeeds()
{
    std::vector<Feed> ret;

    sqlite3_stmt* stmt = _ps_load_active_feeds;

    while(sqlite3_step(stmt) == SQLITE_ROW)
    {
        Feed f = fillFeedFromRow(stmt);
        ret.push_back(f);
    }

    sqlite3_reset(stmt);
    sqlite3_clear_bindings(stmt);

    return ret;
}

std::string DB::formatInsert(const std::string& field)
{
    std::string ret{};

    if(!field.empty())
    {
        ret = "'" + field + "',";
    }
    else
    {
        ret = "NULL,";
    }

    return ret;
}

std::string DB::formatInsert(const bool field)
{
    std::string ret{};

    if(field)
    {
        ret = "1,";
    }
    else
    {
        ret = "0,";
    }

    return ret;
}

db_result DB::insertArticle(const Article &a)
{
    sqlite3_stmt* stmt = _ps_insert_article;

    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$pkey"), a.pkey.c_str(), strlen(a.pkey.c_str()), nullptr);
    sqlite3_bind_int(stmt, sqlite3_bind_parameter_index(stmt, "$feed_key"), a.feed_key);
    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$legacy_id"), a.legacy_id.c_str(), strlen(a.legacy_id.c_str()), nullptr);
    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$published_at"), a.published_at.c_str(), strlen(a.published_at.c_str()), nullptr);
    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$updated_at"), a.published_at.c_str(), strlen(a.published_at.c_str()), nullptr);
    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$source"), a.source.c_str(), strlen(a.source.c_str()), nullptr);
    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$title"), a.title.c_str(), strlen(a.title.c_str()), nullptr);
    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$lead"), a.lead.c_str(), strlen(a.lead.c_str()), nullptr);
    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$text"), a.text.c_str(), strlen(a.text.c_str()), nullptr);
    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$category"), a.category.c_str(), strlen(a.category.c_str()), nullptr);

    db_result ret;
    ret.ret_code = sqlite3_step(stmt);
    if(ret.ret_code != SQLITE_DONE)
    {
        ret.message = sqlite3_errmsg(_db);
        ret.query = sqlite3_sql(stmt);
    }

    sqlite3_reset(stmt);
    sqlite3_clear_bindings(stmt);

    return ret;
}

db_result DB::updateFeed(const Feed& f)
{
    db_result ret;

    if(f.pkey.length() <= 0)
    {
        ret.message = "need a pkey to update";
        ret.ret_code = 12;
        return ret;
    }

    // TODO update feeds if there is an error during parsing etc
    sqlite3_stmt* stmt = _ps_update_feed;

    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$pkey"), f.pkey.c_str(), strlen(f.pkey.c_str()), nullptr);
    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$last_error"), f.last_error.c_str(), strlen(f.last_error.c_str()), nullptr);
    sqlite3_bind_text(stmt, sqlite3_bind_parameter_index(stmt, "$last_content"), f.last_content.c_str(), strlen(f.last_content.c_str()), nullptr);
    sqlite3_bind_int(stmt, sqlite3_bind_parameter_index(stmt, "$fail_count"), f.fail_count);


    ret.ret_code = sqlite3_step(stmt);
    if(ret.ret_code != SQLITE_DONE)
    {
        ret.message = sqlite3_errmsg(_db);
        ret.query = sqlite3_sql(stmt);
    }

    sqlite3_reset(stmt);
    sqlite3_clear_bindings(stmt);

    return ret;
}

db_result DB::beginTransaction()
{
    db_result ret;
    char* messageError;

    ret.ret_code = sqlite3_exec(_db, "BEGIN TRANSACTION;", nullptr, nullptr, nullptr);

    if (ret.ret_code != SQLITE_OK)
    {
        ret.message = messageError;
        sqlite3_free(messageError);
    }

    return ret;
}

db_result DB::commitTransaction()
{
    db_result ret;
    char* messageError;

    ret.ret_code = sqlite3_exec(_db, "COMMIT TRANSACTION;", nullptr, nullptr, &messageError);
    if (ret.ret_code != SQLITE_OK)
    {
        ret.message = messageError;
        sqlite3_free(messageError);
    }

    return ret;
}

db_result DB::rollbackTransaction()
{
    db_result ret;
    char* messageError;

    ret.ret_code = sqlite3_exec(_db, "ROLLBACK TRANSACTION;", nullptr, nullptr, &messageError);
    if (ret.ret_code != SQLITE_OK)
    {
        ret.message = messageError;
        sqlite3_free(messageError);
    }

    return ret;
}

std::string DB::getNowAsSQliteDate()
{
    const auto now = std::chrono::system_clock::now();
    return fmt::format("{:%Y-%m-%d %H:%M:%S}", now);
}

Feed DB::fillFeedFromRow(sqlite3_stmt* stmt)
{
    Feed f{};
    fillIfNotNullText(stmt, static_cast<int>(FEED_FIELDS_SHORT::PKEY), &f.pkey);
    f.feed_key = sqlite3_column_int(stmt, static_cast<int>(FEED_FIELDS_SHORT::FEED_KEY));
    fillIfNotNullText(stmt, static_cast<int>(FEED_FIELDS_SHORT::URL), &f.url);
    fillIfNotNullText(stmt, static_cast<int>(FEED_FIELDS_SHORT::USER_AGENT), &f.user_agent);
    fillIfNotNullText(stmt, static_cast<int>(FEED_FIELDS_SHORT::POLL_FREQUENCY), &f.poll_freq);

    f.fail_count = sqlite3_column_int(stmt, static_cast<int>(FEED_FIELDS_SHORT::FAIL_COUNT));
    f.give_up_at = sqlite3_column_int(stmt, static_cast<int>(FEED_FIELDS_SHORT::GIVE_UP_AT));
    f.truncate_parameters = sqlite3_column_int(stmt, static_cast<int>(FEED_FIELDS_SHORT::TRUNC_PARAMS));

    return f;
}

void DB::fillIfNotNullText(sqlite3_stmt* stmt, int column, std::string* field_to_fill)
{
    const char* val = reinterpret_cast<const char*>(sqlite3_column_text(stmt, column));
    if(val != nullptr)
    {
        field_to_fill->assign(std::string(val));
    }
}
// EOF